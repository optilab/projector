// projector.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <pylon/PylonIncludes.h>
#ifdef PYLON_WIN_BUILD
#    include <pylon/PylonGUI.h>
#endif

#include <opencv2/opencv.hpp> 
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#define GLOBAL_WIDTH 1280.0
#define GLOBAL_HEIGHT 1024.0
// Namespace for using pylon objects.
using namespace Pylon; 
enum State { STATE_IDLE, STATE_SHOW_WHITE, STATE_CALIBRATE, STATE_SIMPLE_DRAW, STATE_TIME_DRAW, STATE_CLEAR };
State state = STATE_IDLE;
State prevState = STATE_IDLE;

cv::Point2f find_point(cv::Mat & currentImage, cv::Mat & lambda)
{
	cv::Mat color_planes[3];
	cv::split(currentImage, color_planes);
	cv::Mat green;
	std::vector<std::vector<cv::Point> > contours;
	std::vector<cv::Vec4i> hierarchy;
	std::vector<cv::Point2f> approx;
	color_planes->copyTo(green);
	cv::threshold(green, green, 255 * 0.7, 255, CV_THRESH_BINARY);

	cv::findContours(green, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

	// find max countour
	int i_of_max_contour = -1;
	double max_area = -1;
	for (int i = 0; i < contours.size(); i++)
	{
		auto area = cv::contourArea(contours.at(i));
		if (cv::contourArea(contours.at(i)) > max_area)
		{
			max_area = area;
			i_of_max_contour = i;
		}
	}
	std::vector<cv::Point2f> vec, vecT;
	if (contours.size() > 0)
	{
		auto cntr = contours.at(i_of_max_contour);
		auto M = cv::moments(cntr);
		cv::Point2f p(M.m10 / M.m00, M.m01 / M.m00);

		vec.push_back(p);
		cv::perspectiveTransform(vec, vecT, lambda);
		return vecT.at(0);	
	}
	return cv::Point2f(-1,-1);
}
int _tmain(int argc, _TCHAR* argv[])
{
	double max_intens = 0;
	double min_intens = 0;
	cv::Mat scaledDown;
	Pylon::PylonAutoInitTerm autoInitTerm;
	cv::namedWindow("main", cv::WINDOW_KEEPRATIO);
	cv::Mat white, background;
	cv::setWindowProperty("main", cv::WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);

	std::vector<cv::Point2f> memory(10);
	std::vector<cv::Point2f> cal_points;

	cal_points.push_back(cv::Point2f(0.0, GLOBAL_HEIGHT));
	cal_points.push_back(cv::Point2f(0.0, 0.0));
	cal_points.push_back(cv::Point2f(GLOBAL_WIDTH, 0.0));
	cal_points.push_back(cv::Point2f(GLOBAL_WIDTH, GLOBAL_HEIGHT));

	try
	{
		CInstantCamera camera(CTlFactory::GetInstance().CreateFirstDevice());
		std::cout << "Using device " << camera.GetDeviceInfo().GetModelName() << std::endl;

		GenApi::INodeMap& nodemap = camera.GetNodeMap();

		camera.MaxNumBuffer = 5;

		CImageFormatConverter formatConverter;
		formatConverter.OutputPixelFormat = PixelType_BGR8packed;

		CPylonImage pylonImage;
		int grabbedImages = 0;

		std::cout << "Grab using SW trigger:" << std::endl << std::endl;
		// Register the standard configuration event handler for setting up the camera for software
		// triggering.
		// The current configuration is replaced by the software trigger configuration by setting the
		// registration mode to RegistrationMode_ReplaceAll.
		camera.RegisterConfiguration(new CSoftwareTriggerConfiguration, RegistrationMode_ReplaceAll, Cleanup_Delete);
	
		camera.Open();
		GenApi::CIntegerPtr cam_width = nodemap.GetNode("Width");
		GenApi::CIntegerPtr cam_height = nodemap.GetNode("Height");

		std::cout << "current frame size(WxH)   : " << cam_width->GetValue() << "x" << cam_height->GetValue()  << std::endl;
		std::cout << "change frame size(WxH) to : " << GLOBAL_WIDTH << "x" << GLOBAL_HEIGHT << std::endl;

		cam_width->SetValue(GLOBAL_WIDTH);
		cam_height->SetValue(GLOBAL_HEIGHT);

		camera.StartGrabbing();

		CGrabResultPtr ptrGrabResult;
		cv::Mat thresh, thresh_contours;
		std::vector<std::vector<cv::Point> > contours;
		std::vector<cv::Vec4i> hierarchy;
		std::vector<cv::Point2f> approx;
		
		cv::Mat currentImage, currentImageGS;
		cv::Mat transform_img(cv::Size(cam_width->GetValue(), cam_height->GetValue()), CV_8UC3);
		cv::Mat lambda;
		
		while (camera.IsGrabbing())
		{
		auto t1 = cv::getTickCount();
			camera.WaitForFrameTriggerReady(100, TimeoutHandling_ThrowException);
			camera.ExecuteSoftwareTrigger();
			camera.RetrieveResult(5000, ptrGrabResult, TimeoutHandling_ThrowException);

			if (ptrGrabResult->GrabSucceeded())
			{
				formatConverter.Convert(pylonImage, ptrGrabResult);
//				std::cout << ptrGrabResult->GetHeight() << " x " << ptrGrabResult->GetWidth() << std::endl;
				currentImage = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3, (uint8_t*)pylonImage.GetBuffer());


				switch (state)
				{
				case STATE_IDLE:
					cv::imshow("main", currentImage);
					break;
				case STATE_SHOW_WHITE:
					white = cv::Mat::ones(currentImage.size(), CV_8U) * 255;
					cv::imshow("main", white);
					break;
				case STATE_CALIBRATE:
				{
					cv::cvtColor(currentImage, currentImageGS, cv::COLOR_BGR2GRAY);
					cv::minMaxLoc(currentImageGS, &min_intens, &max_intens);
					cv::threshold(currentImageGS, thresh, max_intens*0.5, 255, cv::THRESH_BINARY);
					thresh.copyTo(thresh_contours);

					cv::imshow("main", thresh);
					cv::waitKey(0);

					cv::findContours(thresh_contours, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_NONE, cv::Point(0, 0));

					// find max countour
					int i_of_max_contour = 0;
					double max_area = -1;
					for (int i = 0; i < contours.size(); i++)
					{
						auto area = cv::contourArea(contours.at(i));
						if (cv::contourArea(contours.at(i)) > max_area)
						{
							max_area = area;
							i_of_max_contour = i;
						}
					}

					auto cntr = contours.at(i_of_max_contour);

					double d = 0;
					do
					{
						d = d + 1;
						approxPolyDP(contours[i_of_max_contour], approx, d, true);
					} while (approx.size() > 4);

					std::vector<cv::Point2f> outputQuad;
					
 					for (auto a : approx)
					{
						std::vector<double> dist;
						for (auto c : cal_points)
						{
							dist.push_back(std::sqrt(std::pow(a.x - c.x, 2) + std::pow(a.y - c.y, 2)));
						}
						int ind = std::min_element(dist.begin(), dist.end()) - dist.begin();
						outputQuad.push_back(cal_points.at(ind));
					}

					lambda = cv::getPerspectiveTransform(approx, outputQuad);
					background = cv::Mat::zeros(currentImage.size(), CV_8UC3);
					state = STATE_SIMPLE_DRAW;
					break;
				}

				case STATE_SIMPLE_DRAW:
				{
					if (lambda.cols != 0)
					{
						auto p = find_point(currentImage, lambda);
						cv::circle(background, p, 10, cv::Scalar(0, 0, 255), -1);
						cv::resize(background, scaledDown, background.size()/2, 1, 1);
						cv::imshow("main", background);
					}
					break;
				}
				case STATE_TIME_DRAW:
				{
					if (lambda.cols != 0)
					{
						auto p = find_point(currentImage, lambda);
						if (p.x > 0)
						{
							memory.push_back(p);
							if (memory.size() > 20)
							{
								memory.erase(memory.begin());
							}
						}
						background = cv::Mat::zeros(currentImage.size(), CV_8UC3);
						for (int i = 0; i < memory.size(); i++)
						{
							cv::circle(background, memory.at(i), i, cv::Scalar(0, 0, 255), -1);
						}
						cv::resize(background, scaledDown, background.size()/2, 1, 1);
						cv::imshow("main", background);
					}
					break;
				}
				case STATE_CLEAR:{
					background = cv::Mat::zeros(currentImage.size(), CV_8UC3);
					memory.clear();
					state = prevState;
					break;
				}
				}
				
				int key = cv::waitKey(1);
				
				if (key == 'q')
				{
					state = STATE_IDLE;
					camera.StopGrabbing();
					break;
				}else if (key == ' ')
				{
					state = STATE_CALIBRATE;
				}
				else if (key == 'w')
				{
					state = STATE_SHOW_WHITE;
				}
				else if (key == 't')
				{
					state = STATE_TIME_DRAW;
				}
				else if (key == 'd')
				{
					state = STATE_SIMPLE_DRAW;
				}
				else if (key == 'c')
				{
					if (state == STATE_SIMPLE_DRAW | state == STATE_TIME_DRAW)
					{
						prevState = state;
						state = STATE_CLEAR;
					}
				}

			}
			std::cout << "fps " << 1/ (double(cv::getTickCount() - t1)/cv::getTickFrequency()) << std::endl;
}

		camera.Close();

	} catch (...)
	{
		std::cerr << "Somthing went wrong!" << std::endl;
	}
	return 0;
}

